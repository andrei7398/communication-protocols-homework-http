#include <stdlib.h>     /* exit, atoi, malloc, free */
#include <stdio.h>
#include <unistd.h>     /* read, write, close */
#include <string.h>     /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h>      /* struct hostent, gethostbyname */
#include <arpa/inet.h>
#include "helpers.h"
#include "requests.h"

char *compute_request(char *method, char *url, char *url_params, char *host, char *authorization, char *type, char *cookies, char *form_data) {

    char *message = calloc(BUFLEN, sizeof(char));
    char *line = calloc(LINELEN, sizeof(char));
    
    // Write method name,url and data to post
    if (strcmp(method,"GET") == 0) {
        if (url_params != NULL) {
            sprintf(line, "GET %s?%s HTTP/1.1", url, url_params);
        }
        else {
            sprintf(line, "GET %s HTTP/1.1", url);
        }
    }
    else {
        if (url != NULL) {
            sprintf(line, "POST %s HTTP/1.1", url);
        }
        else {
            sprintf(line, "POST / HTTP/1.1");
        }
    }
    compute_message(message, line);

    // Add host
    sprintf(line, "Host: %s", host);
    compute_message(message, line);

    // Add authotization header if exists
    if (authorization != NULL) {
        sprintf(line, "Authorization: Basic %s", authorization);
        compute_message(message, line);
    }

    if (strcmp(method,"POST") == 0) {
        // Always add Content-Type - json/plain text
        sprintf(line, "Content-Type: %s", type);
        compute_message(message, line);
        
        // Always add Content-Length
        sprintf(line, "Content-Length: %ld", strlen(form_data));
        compute_message(message, line);
    }
    
    // Add cookies if any
    if (cookies != NULL) {
        sprintf(line, "Cookie: %s", cookies);
        compute_message(message, line);
    }

    // Add final empty data
    char empty_line[10] = "";
    compute_message(message, empty_line);

    if (form_data != NULL) {
        // Add data to post
        compute_message(message, form_data);
    }

    free(line);
    return message;
}
