#include <stdlib.h>     /* exit, atoi, malloc, free */
#include <stdio.h>
#include <unistd.h>     /* read, write, close */
#include <string.h>     /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h>      /* struct hostent, gethostbyname */
#include <arpa/inet.h>
#include "helpers.h"

void error(const char *msg)
{
    perror(msg);
    exit(0);
}
void compute_message(char *message, const char *line)
{
    strcat(message, line);
    strcat(message, "\r\n");
}
int open_connection(char *host_ip, int portno /* 80 */, int ip_type /* AF_INET */, int socket_type /* SOCK_STREAM */, int flag /* 0 */)
{
    struct sockaddr_in serv_addr;
    int sockfd = socket(ip_type, socket_type, flag);

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = ip_type;
    serv_addr.sin_port = htons(portno);
    inet_aton(host_ip, &serv_addr.sin_addr);
    if (sockfd < 0)
        error("ERROR opening socket");

    /* connect the socket */
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR connecting");

    return sockfd;
}

void close_connection(int sockfd)
{
    close(sockfd);
}
void send_to_server(int sockfd, char *message)
{
    int bytes, sent = 0;
    int total = strlen(message);
    do
    {
        bytes = write(sockfd, message + sent, total - sent);
        if (bytes < 0)
            error("ERROR writing message to socket");
        if (bytes == 0)
            break;
        sent += bytes;
    } while (sent < total);
}

char *receive_from_server(int sockfd)
{
    char *response = calloc(BUFLEN, sizeof(char));
    int total = BUFLEN;
    int received = 0;
    do
    {
        int bytes = read(sockfd, response + received, total - received);
        if (bytes < 0)
            error("ERROR reading response from socket");
        if (bytes == 0)
        {
            break;
        }
        received += bytes;
    } while (received < total);

    if (received == total)
        error("ERROR storing complete response from socket");

    return response;
}

// char *get_all_cookies_together(char *response) 
// {
//     if (strstr(response,"Set-Cookie:") == NULL) {
//         return '\0';
//     }
//     else {
//         char *cookies = malloc(strlen(response) + 1);
//         char *ptr1, *ptr2;
//         char *aux_response2 = malloc(strlen(response) + 1);
//         char *aux_response1 = malloc(strlen(response) + 1);

//         strcpy(aux_response2,response);

//         aux_response1 = strstr(response,"prajiturica=");
//         ptr1 = strchr(aux_response1, ' ') ;
//         if (ptr1 != NULL) {
//             *ptr1 = '\0';
//         }

//         strcpy(cookies,aux_response1);

//         aux_response2 = strstr(aux_response2,"prajiturica.sig=");
//         ptr2 = strchr(aux_response2, ';') ;
//         if (ptr2 != NULL) {
//             *ptr2 = '\0';
//         }

//         strcat(cookies,aux_response2);

//         return cookies;
//     }
// }
char *get_all_cookies_together(char *response) 
{
    if (strstr(response,"Set-Cookie:") == NULL) {
        return '\0';
    }
    else {
        char *cookies = malloc(strlen(response) + 1);
        char *ptr1, *ptr2;
        char *aux_response2 = malloc(strlen(response) + 1);
        char *aux_response1 = malloc(strlen(response) + 1);

        sprintf(aux_response2, "%s", strstr(response,"Set-Cookie:"));
        aux_response2 = aux_response2 + 12;

        sprintf(aux_response1, "%s", strstr(response,"Set-Cookie:"));
        aux_response1 = aux_response1 + 12;
        ptr1 = strchr(aux_response1, ' ') ;
        if (ptr1 != NULL) {
            *ptr1 = '\0';
        }

        strcpy(cookies,aux_response1);

        aux_response2 = strstr(aux_response2,"prajiturica.sig=");
        ptr2 = strchr(aux_response2, ';') ;
        if (ptr2 != NULL) {
            *ptr2 = '\0';
        }

        strcat(cookies," ");
        strcat(cookies,aux_response2);

        return cookies;
    }
}