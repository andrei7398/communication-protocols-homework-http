#ifndef _REQUESTS_
#define _REQUESTS_

// char *compute_get_request(char *host, char *url, char *url_params, char *authorization, char *cookies);
// char *compute_post_request(char *host, char *url, char *form_data, char *type, char *authorization, char *cookies);
char *compute_request(char *method, char *url, char *url_params, char *host, char *authorization, char *type, char *cookies, char *form_data);

#endif