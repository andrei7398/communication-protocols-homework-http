SFETCU IULIAN-ANDREI - 322CD - TEMA 3 PC

Am folosit biblioteca parson pentru lucrul cu fisierele JSON.
De asemenea am folosit si helpers.c/helpers.h preluate din laborator.

Pentru fiecare task:
	- Deschid socket-ul cu open_connection;
	- Formatez orice string de care am nevoie cum ar fi credentialele pentru
	login (credentials) sau parametrii pentru url (url_params/aux_url_params);
	- Creez mesajul cu headerele necesare cu functia compute_request() definita
	in request.c;
	- Trimit mesajul;
	- Receptionez raspunsul si il impart in response (mesajul intreg) si 
	response_body (stringul JSON);
	- Modific valoarea si obiectul actual JSON;
	- Afisez raspunsul;
	- Inchid conexiunea

Obs: Pentru taskul 5 fac acelasi lucru de 2 ori, prima oara in variabile auxiliare 
	(aux_), salvand raspunsul JSON pentru a-l parsa request-ului pe server.

Functia compute_request() :
	- method: verific daca e GET/POST, iar in cazul in care e GET verific daca
	am parametrii pentru request, iar daca da ii adaug in request.
	- host: este adaugat mereu
	- authorization: adaugata doar daca exista (daca nu e null parametrul)
	- daca e POST:
		- adaug Content-Type in functie de parametrul type
		- adaug Content-Length in functie de data
	- cookies: adaug daca exista (daca nu e null parametrul) folosind functia
	get_all_cookies_together() pe care am definit-o in helpers.c
	- adaug linia goala
	- adaug data (form_data) daca exista

parson:
	- json_parse_string() care creaza o valoare dintr-un string
	- json_value_get_object() care creaza un obiect json dintr-o valoare
	- json_object_dothas_value() care verifica daca exista un camp intr-un
	JSON ce se afla intr-un JSON
	- json_object_get_TYPE(camp) care returneaza valoarea din camp in format
	TYPE
	- json_object_dotget_TYPE(camp) acelasi lucru ca json_object_get_TYPE(camp)
	doar ca accepta JSON in JSON ("Ceva.camp")