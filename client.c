#include <stdio.h>      /* printf, sprintf */
#include <stdlib.h>     /* exit, atoi, malloc, free */
#include <unistd.h>     /* read, write, close */
#include <string.h>     /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h>      /* struct hostent, gethostbyname */
#include <arpa/inet.h>
#include "helpers.h"
#include "requests.h"
#include "parson.h"

int main(int argc, char *argv[]) {

    char *message;
    char *response;
    char *response_body;

    int sockfd;

    char *url_params = malloc (BUFLEN*sizeof(char));
    char *authorization = malloc (BUFLEN*sizeof(char));

    char credentials[100];

    struct hostent *hostent;
    struct in_addr ip_addr;

    // Types of data for POST requests
    char json_type[] = "application/json";
    char text_type[] = "application/x-www-form-urlencoded";

	JSON_Value *root_value;
    JSON_Object *commit;

    char *host_ip = "185.118.200.35";





    // TASK 1 - connect to server
	printf("\n--------------- TASK 1 ---------------\n");
    sockfd = open_connection(host_ip, 8081, AF_INET, SOCK_STREAM, 0);

    // Compute request
    message = compute_request("GET", "/task1/start", NULL, host_ip, NULL, text_type, NULL, NULL);

    // Print client message
	printf("%s\n", message);
    send_to_server(sockfd, message);

    // Receive response from server
    response = receive_from_server(sockfd);
    // Response data (JSON string)
    response_body = strchr(response, '{');
    printf("%s\n", get_all_cookies_together(response));

    // Get JSON
	if (response_body != NULL && strcmp(response_body,"Forbidden!") != 0) {
		root_value = json_parse_string(response_body);
		commit = json_value_get_object(root_value);
	}

    // Print server response & close connection
    printf("%s\n", response);
    //printf("%s\n", json_serialize_to_string_pretty(root_value));
    close_connection(sockfd);





    // TASK 2 - login with credentials
    printf("\n\n\n--------------- TASK 2 ---------------\n");
	sockfd = open_connection(host_ip, 8081, AF_INET, SOCK_STREAM, 0);

	sprintf(credentials, "username=%s&password=%s", json_object_dotget_string(commit, "data.username"), json_object_dotget_string(commit, "data.password"));

    // Saving the new authorization, if exists
    if (json_object_dothas_value(commit, "data.token")) {
        authorization = json_object_dotget_string(commit, "data.token");
    }

    // Compute request
    message = compute_request(json_object_get_string(commit, "method"), json_object_get_string(commit, "url"), NULL, 
                host_ip, authorization, text_type, get_all_cookies_together(response), credentials);

    // Print client message
	printf("%s\n", message);
	send_to_server(sockfd, message);

    // Receive response from server
	response = receive_from_server(sockfd);
    // Response data (JSON string)
	response_body = strchr(response, '{');

    // Get JSON
	if (response_body != NULL && strcmp(response_body,"Forbidden!") != 0) {
		root_value = json_parse_string(response_body);
		commit = json_value_get_object(root_value);
	}

    // Print server response & close connection
	printf("%s\n", response);
    //printf("%s\n", json_serialize_to_string_pretty(root_value));
    close_connection(sockfd);





    // TASK 3 - JWT
    printf("\n\n\n--------------- TASK 3 ---------------\n");
	sockfd = open_connection(host_ip, 8081, AF_INET, SOCK_STREAM, 0);

	sprintf(url_params, "raspuns1=omul&raspuns2=numele&id=%s", json_object_dotget_string(commit, "data.queryParams.id"));

    // Saving the new authorization, if exists
    if (json_object_dothas_value(commit, "data.token")) {
        authorization = json_object_dotget_string(commit, "data.token");
    }

    // Compute request
    message = compute_request(json_object_get_string(commit, "method"), json_object_get_string(commit, "url"), url_params, 
                host_ip, authorization, text_type, get_all_cookies_together(response), NULL);

    // Print client message
	printf("%s", message);
	send_to_server(sockfd, message);

    // Receive response from server
	response = receive_from_server(sockfd);
    // Response data (JSON string)
	response_body = strchr(response, '{');

    // Get JSON
	if (response_body != NULL && strstr(response_body,"Forbidden!") == NULL) {
		root_value = json_parse_string(response_body);
		commit = json_value_get_object(root_value);
	}

    // Print server response & close connection
	printf("%s\n", response);
    //printf("%s\n", json_serialize_to_string_pretty(root_value));
    close_connection(sockfd);





    // TASK 4 - GET for a key
    printf("\n\n\n--------------- TASK 4 ---------------\n");
    sockfd = open_connection(host_ip, 8081, AF_INET, SOCK_STREAM, 0);

    // Saving the new authorization, if exists
    if (json_object_dothas_value(commit, "data.token")) {
        authorization = json_object_dotget_string(commit, "data.token");
    }

    // Compute request
    message = compute_request(json_object_get_string(commit, "method"), json_object_get_string(commit, "url"), NULL, 
                host_ip, authorization, text_type, get_all_cookies_together(response), NULL);

    // Print client message
    printf("%s", message);
    send_to_server(sockfd, message);

    // Receive response from server
    response = receive_from_server(sockfd);
    // Response data (JSON string)
    response_body = strchr(response, '{');

    // Get JSON
    if (response_body != NULL && strstr(response_body,"Forbidden!") == NULL) {
        root_value = json_parse_string(response_body);
        commit = json_value_get_object(root_value);
    }

    // Print server response & close connection
    printf("%s\n", response);
    //printf("%s\n", json_serialize_to_string_pretty(root_value));
    close_connection(sockfd);





    // TASK 5 - GET Weather / POST Weather
    // GET WEATHER
    char *aux_site1 = malloc (BUFLEN*sizeof(char));
    char *aux_site2 = malloc (BUFLEN*sizeof(char));
    char *aux_url = malloc (BUFLEN*sizeof(char));
    char *aux_url_params = malloc (BUFLEN*sizeof(char));
    char *aux_message = malloc (BUFLEN*sizeof(char));
    char *aux_response = malloc (BUFLEN*sizeof(char));
    char *aux_response_body = malloc (BUFLEN*sizeof(char));
    char *aux_host_ip = malloc (BUFLEN*sizeof(char));
    char *API = malloc (BUFLEN*sizeof(char));

    // Get IP & URL
    sprintf(aux_site1, "%s", json_object_dotget_string(commit, "data.url"));
    sprintf(aux_site2, "%s", json_object_dotget_string(commit, "data.url"));
    API = strtok(aux_site1, "/");
    aux_url = strchr(aux_site2, '/');

    hostent = gethostbyname(API);
    ip_addr = *(struct in_addr *)(hostent->h_addr);
    aux_host_ip = inet_ntoa (ip_addr);

    printf("\n\n\n--------------- TASK 5 ---------------\n");
    sockfd = open_connection(aux_host_ip, 80, AF_INET, SOCK_STREAM, 0);

    sprintf(aux_url_params, "q=%s&APPID=%s", json_object_dotget_string(commit, "data.queryParams.q"), json_object_dotget_string(commit, "data.queryParams.APPID"));

    // Compute request
    aux_message = compute_request(json_object_dotget_string(commit, "data.method"), aux_url, aux_url_params, 
            aux_host_ip, NULL, text_type, NULL, NULL);


    // Print client message
    printf("%s", aux_message);
    send_to_server(sockfd, aux_message);

    // Receive response from server
    aux_response = receive_from_server(sockfd);
    // Response data
    aux_response_body = strchr(aux_response, '{');

    // Print server response & close connection
    printf("%s\n\n\n", aux_response);
    close_connection(sockfd);



    // POST WEATHER
    sockfd = open_connection(host_ip, 8081, AF_INET, SOCK_STREAM, 0);

    // Compute request
    message = compute_request(json_object_get_string(commit, "method"), json_object_get_string(commit, "url"), NULL, 
                host_ip, authorization, json_type, get_all_cookies_together(response), aux_response_body);

    // Print client message
    printf("%s\n", message);
    send_to_server(sockfd, message);

    // Receive response from server
    response = receive_from_server(sockfd);
    printf("%s\n",  response);

    close_connection(sockfd);

    free(message);
    return 0;
}
